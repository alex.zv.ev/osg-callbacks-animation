#ifndef ANIMATIONCALLBACKS_H
#define ANIMATIONCALLBACKS_H

#include <osg/Drawable>
#include <osg/Geometry>
#include <osg/MatrixTransform>
#include <osg/ShapeDrawable>
#include <iostream>

///
/// \brief SimpleDrawableUpdateCallback - callback для анимации osg::Drawable, постоянно применяющий константную матрицу трансформации.
/// 
class SimpleDrawableUpdateCallback : public osg::Drawable::UpdateCallback
{
public:
    ///
    /// \brief SimpleDrawableUpdateCallback - конструктор, опционально принимающий матрицу трансформации, которая будет применяться.
    /// \param matrix 
    ///
    explicit SimpleDrawableUpdateCallback(const osg::Matrix& matrix = osg::Matrix()) : m_matrix(matrix) {}

    ///
    /// \brief update - Унаследованная функция, которая умножает координаты каждой вершины на матрицу трансформации, храняющуюся в текущем объекте.
    /// \param drawable - osg::Geometry, вершины которого будут изменены. Если передать неподходящий объект или объект, не содержащий вершин, функция ничего не сделает.
    ///
    virtual void update( osg::NodeVisitor*, osg::Drawable* drawable ) override;

private:
    const osg::Matrix m_matrix;
};

///
/// \brief MatrixTransformRotateCallback - callback для анимации osg::MatrixTransform, вращающий фигуру вокруг вертикальной оси.
/// 
class MatrixTransformRotateCallback : public osg::NodeCallback
{
    ///
    /// \brief operator() - Унаследованная функция, вызывающаяся при обновлении объекта. Применяет матрицу вращения к фигуре.
    /// \param node - Вращаемая фигура типа osg::MatrixTransform.
    ///
    virtual void operator()( osg::Node* node, osg::NodeVisitor* ) override;
};

class SinTranslationUpdateCallback : public osg::NodeCallback
{
public:

    ///
    /// \brief operator() - Унаследованная функция, вызывающаяся при обновлении объекта. Перемещает фигуру в соответствии с функцией синуса, вверх-вниз.
    /// \param node - Перемещаемая фигура типа osg::MatrixTransform.
    ///
    virtual void operator()( osg::Node* node, osg::NodeVisitor* ) override;

private:
    osg::Matrix m_matrix = osg::Matrix::translate(0, 0, 0);
    double m_x = 0;
    const double m_speed = 0.06;
    const double m_amplitudeMultiplier = 0.01;

};


#endif // ANIMATIONCALLBACKS_H