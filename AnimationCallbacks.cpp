#include "AnimationCallbacks.h"

///
/// \brief update - Унаследованная функция, которая умножает координаты каждой вершины на матрицу трансформации, храняющуюся в текущем объекте.
/// \param drawable - osg::Geometry, вершины которого будут изменены. Если передать неподходящий объект или объект, не содержащий вершин, функция ничего не сделает.
///
void SimpleDrawableUpdateCallback::update( osg::NodeVisitor*, osg::Drawable* drawable )
{
    osg::Geometry* geom = static_cast<osg::Geometry*>( drawable );
    
    if ( !geom ) return;
    osg::Vec3Array* vertices = static_cast<osg::Vec3Array*>(geom->getVertexArray());
    if ( !vertices ) return;

    for (auto& v : *vertices) {
        v = v * m_matrix;
    }

    geom->dirtyDisplayList();
    geom->dirtyBound();
}

///
/// \brief operator() - Унаследованная функция, вызывающаяся при обновлении объекта. Применяет матрицу вращения к фигуре.
/// \param node - Вращаемая фигура типа osg::MatrixTransform.
///
void MatrixTransformRotateCallback::operator()( osg::Node* node, osg::NodeVisitor* )
{
    osg::MatrixTransform* matrix = dynamic_cast<osg::MatrixTransform*>(node);

    if (!matrix)
        return;

    matrix->setMatrix(matrix->getMatrix() * osg::Matrix::rotate(0.1, 1, 1, 0));
}

///
/// \brief operator() - Унаследованная функция, вызывающаяся при обновлении объекта. Перемещает фигуру в соответствии с функцией синуса, вверх-вниз.
/// \param node - Перемещаемая фигура типа osg::MatrixTransform.
///
void SinTranslationUpdateCallback::operator()( osg::Node* node, osg::NodeVisitor* )
{
    m_x += m_speed;
    m_matrix = osg::Matrix::translate(0, 0, m_amplitudeMultiplier * sin(m_x));

    osg::MatrixTransform* matrix = dynamic_cast<osg::MatrixTransform*>(node);
    if (!matrix)
        return;

    matrix->setMatrix(matrix->getMatrix() * m_matrix);
}
