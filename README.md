# osg-callbacks-animation

### Задание:
> Создать приложение отображающее пары различных фигур на сцене и обеспечивающее их анимацию (не важно какую, движение туда, сюда или по кругу) на базе колбэков с использованием матриц трансформации

### Пример:
<img src=demo/demo.gif></img>

### Сборка и запуск на linux:
```bash
git clone https://gitlab.com/alex.zv.ev/osg-callbacks-animation
cd osg-callbacks-animation
cmake .
make
./osg-callbacks-animation
```