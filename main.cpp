#include <osg/LightSource>
#include <osg/MatrixTransform>
#include <osg/Geometry>
#include <osgUtil/SmoothingVisitor>
#include <osg/ShapeDrawable>
#include <osgViewer/Viewer>
#include "AnimationCallbacks.h"

///
/// \brief createOctahedron - Функция создания октаэдра с вершинами разного цвета.
/// \return Возвращает октаэдр в виде osg::Geometry.
///
osg::Geometry* createOctahedron()
{
    /* Создаем вершины октаэдра */
    osg::ref_ptr<osg::Vec3Array> vertices = new osg::Vec3Array(6);
    (*vertices)[0].set( 0.0f, 0.0f, 1.0f);
    (*vertices)[1].set(-0.5f,-0.5f, 0.0f);
    (*vertices)[2].set( 0.5f,-0.5f, 0.0f);
    (*vertices)[3].set( 0.5f, 0.5f, 0.0f);
    (*vertices)[4].set(-0.5f, 0.5f, 0.0f);
    (*vertices)[5].set( 0.0f, 0.0f,-1.0f);

    /* Устанавливаем цвет для каждой вершины */
    osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
    colors->push_back( osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f) );
    colors->push_back( osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f) );
    colors->push_back( osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f) );
    colors->push_back( osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f) );
    colors->push_back( osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f) );
    colors->push_back( osg::Vec4(0.0f, 1.0f, 1.0f, 1.0f) );

    /* Связываем вершины в треугольники */
    osg::ref_ptr<osg::DrawElementsUInt> indices =
    new osg::DrawElementsUInt(GL_TRIANGLES, 24);
    (*indices)[0] = 0; (*indices)[1] = 1; (*indices)[2] = 2;
    (*indices)[3] = 0; (*indices)[4] = 2; (*indices)[5] = 3;
    (*indices)[6] = 0; (*indices)[7] = 3; (*indices)[8] = 4;
    (*indices)[9] = 0; (*indices)[10] = 4; (*indices)[11] = 1;
    (*indices)[12]= 5; (*indices)[13] = 2; (*indices)[14] = 1;
    (*indices)[15]= 5; (*indices)[16] = 3; (*indices)[17] = 2;
    (*indices)[18]= 5; (*indices)[19] = 4; (*indices)[20] = 3;
    (*indices)[21]= 5; (*indices)[22] = 1; (*indices)[23] = 4;

    /* Создаем геометрию, устанавливая флаг BIND_PER_VERTEX, чтобы цвет устанавливался для каждой вершины отдельно */
    osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
    geom->setVertexArray(vertices.get());
    geom->setColorArray(colors.get());
    geom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
    geom->addPrimitiveSet(indices.get());

    /* Устанавливаем нормали при помощи SmoothingVisitor */
    osgUtil::SmoothingVisitor::smooth(*geom);

    return geom.release();
}

///
/// \brief createLightSource - Функция создания источника света.
/// \param num - Порядковый номер, который будет присвоен свету.
/// \param translation - Вектор перемещения. На эту величину будет смещен источник света.
/// \param color - Цвет, которым будет светиться источник.
/// \return Возвращает osg::MatrixTransform, содержащий osg::LightSource.
///
osg::Node* createLightSource(unsigned int num, const osg::Vec3& translation, const osg::Vec4& color)
{
    /* Создаем свет */
    osg::ref_ptr<osg::Light> light = new osg::Light;
    light->setLightNum(num);
    light->setDiffuse(color);
    light->setPosition(osg::Vec4(0.0f, 0.0f, 0.0f, 1.0f));

    /* Прикрепляем свет к источнику */
    osg::ref_ptr<osg::LightSource> lightSource = new osg::LightSource;
    lightSource->setLight(light); 

    /* Смещаем источник света на заданную величину */
    osg::ref_ptr<osg::MatrixTransform> sourceTrans = new osg::MatrixTransform;
    sourceTrans->setMatrix(osg::Matrix::translate(translation));
    sourceTrans->addChild(lightSource.get());
    return sourceTrans.release();
}

///
/// \brief createQuestMark - Функция создания объемного восклицательного знака, игнорирующего свет. 
/// \param questMarkColor - Цвет фигуры.
/// \return Возвращает osg::Group в виде osg::Node.
///
osg::Node* createQuestMark(const osg::Vec4& questMarkColor = osg::Vec4(1.0, 0.9, 0.0, 1.0))
{
    /* Создаем объемный восклицательный знак (иконка квеста) из простых фигур */
    /* Точка внизу */
    osg::ref_ptr<osg::ShapeDrawable> questDot = new osg::ShapeDrawable;
    questDot->setShape(new osg::Sphere(osg::Vec3(0.0f, 0.0f, -3.5f), 0.7f));
    questDot->setColor(questMarkColor);

    /* Закругленный верх */
    osg::ref_ptr<osg::ShapeDrawable> questUpperSphere = new osg::ShapeDrawable;
    questUpperSphere->setShape(new osg::Sphere(osg::Vec3(0.0f, 0.0f, 0.95f), 1.0f));
    questUpperSphere->setColor(questMarkColor);

    /* Конусообразный центр */
    osg::ref_ptr<osg::ShapeDrawable> questMiddleBody = new osg::ShapeDrawable;
    questMiddleBody->setShape(new osg::Cone(osg::Vec3(0.0f, 0.0f, 0.0f), 1.0f, 1.0f));
    questMiddleBody->setColor(questMarkColor);

    /* Растягиваем конус и переворачиваем его */
    osg::ref_ptr<osg::MatrixTransform> questMiddleBodyTransform = new osg::MatrixTransform;
    questMiddleBodyTransform->setMatrix(osg::Matrix::scale(1, 1, 3) * osg::Matrix::rotate(osg::PI, 1, 0, 0));
    questMiddleBodyTransform->addChild(questMiddleBody);

    /* Объединяем фигуры в группу */
    osg::ref_ptr<osg::Group> questMark = new osg::Group;
    questMark->addChild(questDot);
    questMark->addChild(questUpperSphere);
    questMark->addChild(questMiddleBodyTransform);

    /* Отключаем свет для группы, чтобы добиться эффекта, как в играх */
    questMark->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

    return questMark.release();
}

int main()
{
    /* Создаем октаэдр */
    osg::ref_ptr<osg::Geometry> octahedron = createOctahedron();

    /* Устанавливаем callback для анимации. Фигура будет крутиться вокруг своей оси Z и сжиматься */
    octahedron->setDataVariance( osg::Object::DYNAMIC );
    octahedron->setUpdateCallback(new SimpleDrawableUpdateCallback(osg::Matrix::rotate(0.05, 0, 0, 1) * osg::Matrix::scale(0.9995, 0.9995, 1)));

    /* Создаем зеленый свет с номером 0 */
    osg::ref_ptr<osg::Node> light0 = createLightSource(0, osg::Vec3(0.0f, -5.0f, 0.0f), osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));
    
    /* Добавляем источнику света анимацию */
    light0->setUpdateCallback(new MatrixTransformRotateCallback);

    /* Создаем фигуру восклицательного знака */
    osg::ref_ptr<osg::Node> questMark = createQuestMark();

    /* Приводим полученную группу к нужному размеру и перемещаем её куда надо */
    osg::ref_ptr<osg::MatrixTransform> questTransform = new osg::MatrixTransform;
    questTransform->setMatrix(osg::Matrix::scale(0.3, 0.1, 0.3) * osg::Matrix::translate(0, 0, 3));
    questTransform->addChild(questMark);

    /* Добавляем callback для плавного перемещения фигуры вверх-вниз */
    questTransform->setUpdateCallback(new SinTranslationUpdateCallback);

    osg::ref_ptr<osg::Group> root = new osg::Group;
    root->addChild(questTransform.get());
    root->addChild(octahedron);

    /* Включаем свет под номером 0 */
    root->getOrCreateStateSet()->setMode(GL_LIGHT0, osg::StateAttribute::ON);
    root->addChild(light0);

    osgViewer::Viewer viewer;
    viewer.setSceneData(root.get());
    return viewer.run();
}
